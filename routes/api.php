<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::group([ 'prefix' => 'auth', 'controller' => AuthController::class], function () {
    Route::post('sign-in', 'login');
    Route::post('register', 'register');
});

Route::apiResource('user', UserController::class,['middleware' => 'auth:sanctum']);

Route::group(['prefix' => 'post','middleware' => 'auth:sanctum','controller' => PostController::class,], function () {
    Route::post('search', 'search');
});

Route::apiResource('post', PostController::class,['middleware' => 'auth:sanctum']);
