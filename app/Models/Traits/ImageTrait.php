<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait ImageTrait
{
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value ? env('APP_URL')."/storage/$value" : null,
        );
    }

}
