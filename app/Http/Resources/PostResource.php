<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Post */
class PostResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'user_role' => $this->user_role,
            'image' => $this->image ?? 'https://picsum.photos/seed/picsum/400/300',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
