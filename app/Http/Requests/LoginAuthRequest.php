<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginAuthRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'username' => [
                'required',
                Rule::exists(User::class,'username')
            ],
            'password' => [
                'required'
            ],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
