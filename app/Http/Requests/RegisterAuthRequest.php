<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterAuthRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                'nullable'
            ],
            'username' => [
                'required',
                Rule::unique(User::class,'username')
            ],
            'email' => [
                'required',
                Rule::unique(User::class,'email')
            ],
            'password' => [
                'required'
            ],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'name' => $this->username
        ]);
    }
}
