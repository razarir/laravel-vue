<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginAuthRequest;
use App\Http\Requests\RegisterAuthRequest;
use App\Models\User;
use \Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class AuthController extends Controller
{
    public function login(LoginAuthRequest $request)
    {
        auth()->attempt($request->all());

        if (auth()->check()) {
            return $this->createToken(auth()->user());
        } else {
            return 'username or password is incorrect';
        }
    }

    public function register(RegisterAuthRequest $request)
    {
        $user = User::create($request->validated());

        $user->assignRole(Role::findOrCreate('admin','web'));
        $user->assignRole(Role::findOrCreate('moderator','web'));
        $user->assignRole(Role::findOrCreate('subscriber','web'));
        $user->assignRole(Role::findOrCreate('super_admin','web'));
        $user->assignRole(Role::findOrCreate('filament_user','web'));


        auth()->login($user);

        return $this->createToken($user);

    }

    /**
     * @param User $user
     * @return array
     */
    public function createToken(User $user): array
    {
        $agent = new Agent();
        $tokenName = 'user ' . request()->getClientIp() . ' ' . $agent->deviceType() . ' ' . $agent->platform();
        $token = $user->createToken($tokenName, $user->roles()->pluck('name')->toArray());
        $toArrayUser = auth()->user()->unsetRelation('roles')->toArray();
        return $toArrayUser + ['token' => $token->plainTextToken, 'tokenName' => $tokenName, 'roles' => $token->accessToken->abilities];
    }

}
