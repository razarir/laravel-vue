<?php

namespace App\Http\Controllers;

use App\Events\MessagePosted;
use App\Events\PostPosted;
use App\Http\Requests\PostRequest;
use App\Http\Requests\SearchPostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Post::class);

        return PostResource::collection(Post::all());
    }

    public function store(PostRequest $request)
    {
        $this->authorize('create', Post::class);

        $postResource = new PostResource(Post::create($request->validated()));

        broadcast(new PostPosted(User::first(), $postResource))->toOthers();

        return $postResource;
    }

    public function show(Post $post)
    {
        $this->authorize('view', $post);

        return new PostResource($post);
    }

    public function update(PostRequest $request, Post $post)
    {
        $this->authorize('update', $post);

        $post->update($request->validated());

        return new PostResource($post);
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);

        $post->delete();

        return response()->json();
    }

    public function search(SearchPostRequest $request)
    {
        $this->authorize('viewAny', Post::class);

        $posts = Post::query();

        $posts->when($request->user_role, function ($query, $value) {
            $query->whereUserRole($value);
        });

        $posts = $posts->get();

        return PostResource::collection($posts);
    }
}
