import axios from 'axios';
import authHeader from './auth-header';
import app from '../main';


const instance = axios.create({
    baseURL: import.meta.env.VITE_APP_URL+"/api",
    headers:{
        common: authHeader()
    }
});

instance.interceptors.request.use((config) => {
    app.$Progress.start(); // for every request start the progress
    config.headers['X-Socket-ID'] = Echo.socketId(); // Echo instance
    // the function socketId () returns the id of the socket connection
    return config;
});

instance.interceptors.response.use((response) => {
    app.$Progress.finish(); // finish when a response is received
    return response;
});

export default instance; // export axios instance to be imported in your app






// export default axios;
