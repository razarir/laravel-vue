import axios from './axios';
import app from "../main";
import {swal_alert} from "./global";

const API_URL = 'user';

class UserService {
    index() {
        return axios.get(API_URL)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }

    create(data) {
        return axios.post(API_URL, data)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }

    update(id, data) {
        return axios.put(API_URL + '/' + id, data)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }

    show(id) {
        return axios.get(API_URL + '/' + id)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }

    delete(id) {
        return axios.delete(API_URL + '/' + id)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }

    search(data) {
        return axios.post(API_URL + '/search', data)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });

    }
}

export default new UserService();
