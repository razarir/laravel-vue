import swal from 'sweetalert2'

let swal_alert = {
    success(text) {
        swal.fire({
            title: 'Successful',
            text: text,
            type: 'success',
            confirmButtonText: 'Confirm'
        });
    },
    error(text) {
        swal.fire({
            title: 'Error!',
            text: text,
            type: 'error',
            confirmButtonText: 'Confirm'
        });
    },
    confirm(text) {
        return swal.fire({
            title: 'Are you sure?',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete!',
            cancelButtonText: 'Cancel',
        });
    },
    delete(item) {
        return this.confirm(item + ' to delete?!')
    },
    create(item='') {
        return this.success(item +' added.')
    },
    error_for_all_code(error) {
        switch (error.response.status) {
            case 401:
                this.error('You are not authenticated')
                break
            case 403:
                this.error('Thank you for login')
                break
            case 404:
                this.error('not found')
                break
            case 419:
                this.error('The page has expired. Refresh the page and log in again')
                break
            case 422:
                this.error('The requested information is not entered correctly.')
                return error.response.data.errors;
                break
            case 429:
                this.error('The number of requests you have sent')
                break
            case 500:
                this.error('The server has error')
                break
            case 503:
                this.error('The server is not available')
                break
            case 404:
                this.error('not found')
                break
            default:
                this.error('Error communicating with the server! Please check your internet connection.');
                break
        }
    },
    error_with_code(error) {
        switch (error) {
            case 401:
                this.error('You are not authenticated')
                break
            case 403:
                this.error('Thank you for login')
                break
            case 419:
                this.error('The page has expired. Refresh the page and log in again')
                break
            case 422:
                this.error('The requested information is not entered correctly.')
                break
            case 429:
                this.error('The number of requests you have sent')
                break
            case 500:
                this.error('The server is down')
                break
            case 503:
                this.error('The server is not available')
                break
            case 404:
                this.error('not found')
                break
            default:
                this.error('Error communicating with the server! Please check your internet connection.');
                break
        }
    },
};
let form_data = {
    form_data(form_data = null, files = null, _method = null) {

        let formData = new FormData();
        if (form_data !== null) {
            let let_form_data = {};
            for (const [key, value] of Object.entries(form_data)) {
                if ((value !== null && value !== '')) {
                    if (Array.isArray(value) && value.length === 0) {
                        continue;
                    }
                    let_form_data[key] = value;
                }
            }
            formData.append('data', JSON.stringify(let_form_data));
        }
        if (files !== null && files.length) {
            files.forEach((item) => {
                if (!files.upload && item.file) {
                    formData.append('files' + '[]', item.file);

                }
            });
        }
        if (_method !== null) {
            formData.append('_method', _method);
        }
        return formData;
    }

}

export {swal_alert, form_data};
