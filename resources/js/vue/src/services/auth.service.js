import axios from './axios';
import {swal_alert} from "./global";
import app from "../main";

const API_URL = 'auth/';

class AuthService {
    login(userRequest) {
        return axios
            .post(API_URL + 'sign-in', {
                username: userRequest.username,
                password: userRequest.password
            })
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }

                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });

    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            username: user.username,
            email: user.email,
            password: user.password
        })
            .then(response => {
                app.$Progress.finish();
                return response.data;
            })
            .catch(error => {
                app.$Progress.fail();
                swal_alert.error_for_all_code(error);
                throw error;
            });
    }
}

export default new AuthService();
