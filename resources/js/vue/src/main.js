import {createApp} from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import {FontAwesomeIcon} from './plugins/font-awesome'
import VueProgressBar from "@aacassandra/vue3-progressbar";


// import '@mdi/font/css/materialdesignicons.css'
// import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
    components,
    directives
})

// createApp(Home2)
export default
createApp(App)
    .use(router)
    .use(store)
    .use(vuetify)
    .use(VueProgressBar, {
        color: 'rgb(143, 255, 199)',
        failedColor: 'red',
        height: '15px',
        thickness: '15px',
    })
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount("#app");
