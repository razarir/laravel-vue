import { createWebHistory, createRouter } from "vue-router";
import Home from "./components/Home.vue";
import Home2 from "./components/Home2.vue";
import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
// lazy-loaded
const Post = () => import("./components/Post.vue")
const Profile = () => import("./components/Profile.vue")
const BoardAdmin = () => import("./components/BoardAdmin.vue")
const BoardModerator = () => import("./components/BoardModerator.vue")
const BoardUser = () => import("./components/BoardUser.vue")

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/home",
    name: "home.redirect",
    component: Home,
  },
    {
    path: "/home2",
    name: "home2",
    component: Home2,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/profile",
    name: "profile",
    // lazy-loaded
    component: Profile,
  },
  {
    path: "/admin-board",
    name: "admin",
    // lazy-loaded
    component: BoardAdmin,
  },
  {
    path: "/moderator-board",
    name: "moderator",
    // lazy-loaded
    component: BoardModerator,
  },
  {
    path: "/subscriber-board",
    name: "subscriber",
    // lazy-loaded
    component: BoardUser,
  },
    {
    path: "/post/:id",
    name: "post",
    // lazy-loaded
    component: Post,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register', '/home', '/home2'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});

export default router;
