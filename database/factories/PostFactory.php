<?php

namespace Database\Factories;

use App\Enums\UserRoleEnum;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->words(3,true),
            'description' => $this->faker->paragraphs(3,true),
            'user_role' => collect(['admin','moderator','subscriber'])->random(1)->first(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
